class Player {
    constructor(w, h, s, g) {
        this.xPos = w * 0.05;
        this.yPos = h * 0.5;
        this.size = s;
        this.grav = g;
        this.canvasHeight = h;
        this.isTouchingWall = false;
    }

    draw() {
        this.gravity();
        fill(100, 200, 100);
        ellipse(this.xPos, this.yPos, this.size);
    }

    hasPlayerLost() {
        if (this.isTouchingWall) {
            return true;
        }
        return false;
    }



    gravity() {
        if (this.yPos + this.size < this.canvasHeight) {
            
            if (keyIsDown(UP_ARROW) || keyIsDown(32) || keyIsDown(87)) {
                this.yPos -= this.grav * 0.6;
            }
            else {
                this.yPos += this.grav;
            }
        }
        else {
            alert("You Lose");
        }
        
    }



}