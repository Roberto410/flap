class Wall {
    constructor(isBottomWall, w, h) {
        let halfHeight = h/2 - 10;
        this.height = random(halfHeight * 0.7, halfHeight);
        this.width = 60;
        this.xPos = w;
        this.speed = 5;
        

        if (isBottomWall) {
            this.yPos = (h - this.height);
        }
        else {
            this.yPos = 0;
        }

        this.ulc = [this.xPos, this.yPos];
        this.urc = [this.xPos + this.width, this.yPos];
        this.blc = [this.xPos, this.yPos + this.height];
        this.blc = [this.xPos + this.width, this.yPos + this.height];
    }

    slide() {
        this.xPos -= this.speed;
        this.ulc = [this.xPos, this.yPos];
        this.urc = [this.xPos + this.width, this.yPos];
        this.blc = [this.xPos, this.yPos + this.height];
        this.blc = [this.xPos + this.width, this.yPos + this.height];
    }

    show() {
        fill(100,200,200);
        rect(this.xPos, this.yPos, this.width, this.height);
        this.slide();
    }

    hits(play) {
        if (play.xPos > this.ulc[0] && play.xPos < this.urc[0]) {
            if (play.yPos > this.ulc[1] && play.yPos < this.blc[1]) {
                return true;
            }
            
        }
        return false;
    }
}