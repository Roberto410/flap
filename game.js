
var player;
var framesSinceWall = 0;
var wallList = [];
var numWalls = 0;
var vCanvas;

function setup() {
    frameRate(60);
    vCanvas = createCanvas(1200, 800);
    vCanvas.parent('gameArea');
    player = new Player(width, height, 40, 5);
}



function draw() {
    background(0);
    createWalls();
    
    if (wallList.length > 0) {
        destroyWallsThatLeftTheScreen();
        for (i = 0; i < wallList.length; i++) {
            wallList[i].show();
            if (wallList[i].hits(player)) {
                player.isTouchingWall = true;
            }
        }
    }
    player.draw();
    if (player.hasPlayerLost()) {
        alert("You Lose | Score: " + numWalls * 100);
    }
    
}


function createWalls() {
    if (framesSinceWall > 100) {
        let bottomWall = true;
        for (i = 0; i < 2; i++) {
            let newWall = new Wall(bottomWall, width, height);
            wallList.push(newWall);
            bottomWall = !bottomWall;
        }
        numWalls++;
        framesSinceWall = 0;
    }
    else {
        framesSinceWall++;
    }
}

function destroyWallsThatLeftTheScreen() {
    for (i = 0; i < wallList.length; i++) {
        
        if (wallList[i].xPos < -50) {
            wallList.splice(i,1);
        }
    }
}